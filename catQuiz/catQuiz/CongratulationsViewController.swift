//
//  CongratulationsViewController.swift
//  catQuiz
//
//  Created by はすゆういち on 2019/05/05.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit

class CongratulationsViewController: UIViewController {
    @IBOutlet weak var stageName: UIImageView!
    var id:String?
    var stageNumber:Int?
    var questionNumber:Int?
    var correctAnswer:Int?
    
    
    @IBOutlet weak var Label: UIImageView!
    
    @IBAction func nextPage(_ sender: Any) {
        nextView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setImage()
        // Do any additional setup after loading the view.
    }
    
    
    func setImage(){
        stageName.image = UIImage(named: "Stage \(stageNumber!)_first page_icon")
        if correctAnswer == 5 {
            Label.image = UIImage(named: "Congratulatoins_icon")
        }else{
            Label.frame = CGRect(x: view.frame.width / 2 - 339 * 2 / 2, y: view.frame.height / 2 - 123 * 2 / 2,  width: 339 * 2, height: 123 * 2)
            Label.image = UIImage(named: "Let's try again")
            
        }
    }
    func nextView(){
        if correctAnswer == 5 {
            let storyboard: UIStoryboard = self.storyboard!
            let nextView = storyboard.instantiateViewController(withIdentifier: "Stage") as! StageViewController //次のViewをStoryBoardIDで設定
            nextView.id = id
            self.present(nextView, animated: true, completion: nil)
        }else{
            let storyboard: UIStoryboard = self.storyboard!
            let nextView = storyboard.instantiateViewController(withIdentifier: "Description") as! DescriptionViewController
            nextView.stageNumber = stageNumber!
            nextView.id = id
            self.present(nextView, animated: true, completion: nil)
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
