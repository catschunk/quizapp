//
//  DescriptionViewController.swift
//  catQuiz
//
//  Created by はすゆういち on 2019/05/01.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit
import RealmSwift

class DescriptionViewController: UIViewController {
    @IBOutlet weak var stageName: UIImageView!


    
    var stageNumber:Int?
    let questionNumber = 0
    let correctNumber = 0
    var id:String?
    var sentence1:[String] = [
        "猫を動かして，英文を完成させましょう。",
        "解答時間は１問につき30秒です。",
        "制限時間以内に正解すると魚を１匹ゲット！",
        "魚５匹ゲットで，猫が満腹になりステージクリア！"
    ]
    var sentence2:[String] = [
        "猫を動かして，英文を完成させましょう。",
        "解答時間は１問につき30秒です。",
        "制限時間以内に正解すると猫缶を１個ゲット！",
        "猫缶５匹ゲットで，猫が満腹になりステージクリア！"
    ]
    var lastCheck:[Bool] = [false,false,false,false,false]
    
    //ログ用のデータ
    var entiredata : entireData!
    var realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("RealmURL = \(realm.configuration.fileURL)")
//        print("stageは\(stageNumber!)を選択しました。")
        if stageNumber! == 1 {
            stageName.image = UIImage(named: "Stage 1_first page_icon")
            //日本語
            var Sentence:[UILabel] = []
            for N in 0...3{
                Sentence.append(UILabel())
                Sentence[N].font = UIFont.boldSystemFont(ofSize: 30)
                Sentence[N].text = sentence1[N]
                Sentence[N].frame = CGRect(x: 239, y: 270 + 58 * N, width: 0, height: 40)
                Sentence[N].sizeToFit()
                Sentence[N].textColor = UIColor.blue
                self.view.addSubview(Sentence[N])
            }
            
        }else{
            stageName.image = UIImage(named: "Stage 2_first page_icon")
            //日本語
            var Sentence:[UILabel] = []
            for N in 0...3{
                Sentence.append(UILabel())
                Sentence[N].font = UIFont.boldSystemFont(ofSize: 30)
                Sentence[N].text = sentence2[N]
                Sentence[N].frame = CGRect(x: 239, y: 270 + 58 * N, width: 0, height: 40)
                Sentence[N].sizeToFit()
                Sentence[N].textColor = UIColor.blue
                self.view.addSubview(Sentence[N])
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let QuizView = segue.destination as! QuizViewController
        QuizView.stageNumber = stageNumber
        QuizView.questionNumber = questionNumber
        QuizView.correctAnswer = correctNumber
        QuizView.checkCorrect = lastCheck
        QuizView.entiredata = entiredata
        QuizView.firstFlag = true
        QuizView.realm = realm
        QuizView.id = id
    }

}
