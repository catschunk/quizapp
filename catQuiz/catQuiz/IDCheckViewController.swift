//
//  IDCheckViewController.swift
//  catQuiz
//
//  Created by はすゆういち on 2019/05/01.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit

class IDCheckViewController: UIViewController {

    @IBOutlet weak var IDText: UITextField!
    @IBAction func startButton(_ sender: Any) {
        let storyboard: UIStoryboard = self.storyboard!
        let nextView = storyboard.instantiateViewController(withIdentifier: "Stage") as! StageViewController //次のViewをStoryBoardIDで設定
        nextView.id = IDText.text!
        self.present(nextView, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("IDCheck")
    }

}
