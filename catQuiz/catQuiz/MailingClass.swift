//
//  MailingClass.swift
//  catQuiz
//
//  Created by 保浦良太 on 2019/07/09.
//  Copyright © 2019年 lab. All rights reserved.
//

import UIKit
import MessageUI
import RealmSwift

class MailingClass: NSObject, MFMailComposeViewControllerDelegate{
    
    var csvData=[[String]]()
    
    
    func StartMailer(realm:Realm,VC:UIViewController) {
        //メールを送信できるかチェック
        if MFMailComposeViewController.canSendMail()==false {
            print("Email Send Failed")
            return
        }
        
//        //realmのデータ読み込み
//        let log = realm.objects(entireData.self)
//        print(log)
//
        sendMailWithCSV(subject: "ログ", message: "このまま送信してください。", realm: realm, VC: VC)
        
    }
    
    func sendMailWithCSV(subject: String, message: String, realm:Realm, VC:UIViewController) {
        
        let mailViewController = MFMailComposeViewController()
        mailViewController.mailComposeDelegate = self
        var toRecipients = ["drgunz112@gmail.com"]
        mailViewController.setSubject(subject)
        mailViewController.setToRecipients(toRecipients)
        mailViewController.setMessageBody(message, isHTML: false)
        do {
            //realmファイルの送信
            //TODO: mimeTypeの設定 "text/csv" ? (バイナリフィルの指定がわからない...)
            let data = try Data(contentsOf: realm.configuration.fileURL!)
            mailViewController.addAttachmentData(data, mimeType: "realm/binary", fileName: "default.realm")
        } catch {
            print("error ...")
        }
        VC.present(mailViewController,animated: true)
    
    }
    
    
}
