//
//  QuizViewController.swift
//  catQuiz
//
//  Created by はすゆういち on 2019/05/04.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit
import RealmSwift
import AVFoundation


class QuizViewController: UIViewController,TimeManageDelegate,UIGestureRecognizerDelegate,AVAudioPlayerDelegate {

    //音声再生用
    var audioPlayer: AVAudioPlayer!
    
    
    var stageNumber:Int?
    var questionNumber:Int?
    var correctAnswer:Int?
    var checkCorrect:[Bool]?
    var id:String?
    
    //ログ用のデータ
    var realm : Realm!
    var entiredata : entireData!
    var progressdata = progressData()
    var firstFlag:Bool!
    
    //時間
    var TimeManager = TimerClass()
    
    // スクリーンサイズの取得
    var screenW:CGFloat = 0.0
    var screenH:CGFloat = 0.0
    

    //猫の画像表示(最高で８匹表示)
    var Image:[UIImage] = []
    var ImageView:[UIImageView] = []
    //座標用(imageX,imageY)
    var imageX:[CGFloat] = []
    var imageY:[CGFloat] = []
    var shuffleImageX:[CGFloat] = []
    
    var checkAndJudge:[[Int]] = []
    
    //猫缶と魚どっちか
    var nekofish:UIImage!
    //制限時間用の猫缶
    @IBOutlet weak var Nekocan1: UIImageView!

    let EnglishSentence1 = [
        ["Q1:  Lake District","＿＿＿＿","＿＿＿＿ ."],
        ["Q2:  I"," ＿＿＿＿"," ＿＿＿＿"," ＿＿＿＿ ."],
        ["Q3:  I"," ＿＿＿＿"," ＿＿＿＿"," ＿＿＿＿ ."],
        ["Q4:  I"," ＿＿＿＿"," ＿＿＿＿"," ＿＿＿＿ ."],
        ["Q5:  I couldn't"," ＿＿＿＿"," ＿＿＿＿"," ＿＿＿＿ ."]
    ]
    let JapaneseSentence1 = [
        "湖水地方はとても美しい場所です。",
        "私はあなたのようにドラムを演奏したいです。",
        "私は今日，特にすることがありません。",
        "私は音楽を聴きながら数学の宿題をやりました。",
        "私は探していたCDを見つけることができませんでした。"]
    let EnglishSentence2 = [
        ["Q1:  Ichiro","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿"],
        ["Q2:  I","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿ ."],
        ["Q3:  I","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿ ."],
        ["Q4:  I","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿", "a bath."],
        ["Q5:  I","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿","＿＿＿＿ ."],
    ]
    let EnglishSentenscSub = "I ＿＿＿＿ ＿＿＿＿ ＿＿＿＿ ＿＿＿＿ "
    let JapaneseSentence2 = [
        "イチローはとても有名な野球選手です。",
        "私はあなたのように上手に絵を描きたいです。",
        "あなたに伝えるべき大事なことはありません。",
        "私は風呂に入りながら音楽を聴きました。",
        "私は探していた本を見つけることができませんでした。"
    ]
    
    
    @IBOutlet weak var JapaneseText: UILabel!
    @IBOutlet weak var EnglishText1: UILabel!
    @IBOutlet weak var subText: UILabel!
    
    @IBOutlet weak var blankView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("checkCorrect:\(checkCorrect!)")
        
        screenW = view.frame.size.width
        screenH = view.frame.size.height
//        print("(ScreenH,ScreenW)=(\(screenH),\(screenW))")
        
        //問題番号更新
        questionNumber = questionNumber! + 1
        //print("stage:\(stageNumber!),question:\(questionNumber!),correct:\(correctAnswer!)")
        setImage()
        
        //タイマーのスタート
        TimeManager.timedelegate = self
        TimeManager.startQuestionTimer()
        //entireDataの初期設定
        entiredata = entireData()
        entiredata.id = id!
        entiredata.stage = "\(stageNumber!)"
        entiredata.question = "\(questionNumber!)"
        entiredata.startTime = TimeManager.nowtimeGet()

        
        
    }

    func setImage(){
        //=====================ステージごとの日本語と英文の表示=========================
        var Jsentence:[String] = []
        var Esentence:[[String]] = [[]]
        
        //コード短めにするために切り替える
        switch stageNumber {
        case 1:
            Jsentence = JapaneseSentence1
            Esentence = EnglishSentence1
            //猫缶or魚の設定
            nekofish = UIImage(named: "fish.png")
            Nekocan1.image = nekofish
        case 2:
            Jsentence = JapaneseSentence2
            Esentence = EnglishSentence2
            nekofish = UIImage(named: "neko can.png")
            Nekocan1.image = nekofish
        default: break
        }
        
        JapaneseText.text = Jsentence[questionNumber! - 1]
        EnglishText1.text = Esentence[questionNumber! - 1][0]
        EnglishText1.numberOfLines = 1
        EnglishText1.sizeToFit()
//        EnglishText1.backgroundColor = .white
        var xpoint :CGFloat = EnglishText1.frame.origin.x + EnglishText1.frame.width + 5
        let ypoint :CGFloat = EnglishText1.frame.origin.y
        
        var blankNumber = 0
        var blanklabel:[UILabel] = []
        for (index, blank) in Esentence[questionNumber! - 1].enumerated(){
            if index != 0 {
                if blank.contains("＿＿＿＿"){
                    blanklabel.append(UILabel())
                    blanklabel[blankNumber].font = EnglishText1.font
                    blanklabel[blankNumber].text = blank
                    blanklabel[blankNumber].sizeToFit()
                    
                    blanklabel[blankNumber].frame.origin.x = xpoint
                    blanklabel[blankNumber].frame.origin.y = ypoint
                    //                blanklabel[blankNumber].backgroundColor = .gray
                    blankView.addSubview(blanklabel[blankNumber])
                    xpoint = blanklabel[blankNumber].frame.origin.x + blanklabel[blankNumber].frame.width + 5
                    blanklabel[blankNumber].tag = blankNumber
                    blankNumber = blankNumber + 1
                    
                }else{
                    let notBlanklabel = UILabel()
                    notBlanklabel.font = EnglishText1.font
                    notBlanklabel.text = blank
                    notBlanklabel.sizeToFit()
                    
                    notBlanklabel.frame.origin.x = xpoint
                    notBlanklabel.frame.origin.y = ypoint
                    self.view.addSubview(notBlanklabel)
                    xpoint = notBlanklabel.frame.origin.x + notBlanklabel.frame.width + 5
                }
            }
        }//for
        
        //========================猫の表示最大８匹表示＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
        for nekoNumber in 0..<7 {
            //猫のn番目の画像がある確認
//            let nekoImage = UIImage(named: "Stage \(stageNumber!)_Q\(questionNumber!)/cat_S\(stageNumber!)_Q\(questionNumber!)_\(nekoNumber + 1).png")
            
            let nekoImage = UIImage(named: "Neko_Chunk_Cats _NEW/Stage \(stageNumber!)_Q\(questionNumber!)_\(nekoNumber + 1).png")
            
            if nekoImage != nil{
                //もし猫の画像があるなら追加する
//                Image.append(UIImage(named: "Stage \(stageNumber!)_Q\(questionNumber!)/cat_S\(stageNumber!)_Q\(questionNumber!)_\(nekoNumber + 1).png")!)
                Image.append(UIImage(named: "Neko_Chunk_Cats _NEW/Stage \(stageNumber!)_Q\(questionNumber!)_\(nekoNumber + 1).png")!)
                
                ImageView.append(UIImageView())
                ImageView[nekoNumber] = UIImageView(image: Image[nekoNumber])
                ImageView[nekoNumber].frame = CGRect(x: 50 + nekoNumber * 110 ,y: 500 , width: 120, height: 120)
                //座標の追加
                imageX.append(ImageView[nekoNumber].center.x)
                imageY.append(ImageView[nekoNumber].center.y)
                print("x:\(imageX[nekoNumber]),y:\(imageY[nekoNumber])")
                ImageView[nekoNumber].tag = nekoNumber
                ImageView[nekoNumber].isUserInteractionEnabled = true
//                self.view.addSubview(ImageView[nekoNumber])
                
                //画像ごとの正答数と試行回数用
                //[正解なら１,施行した回数]
                checkAndJudge.append([0,0])
                
                //Pan操作をImageViewに登録
                let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panMove(sender:)))
                panGesture.delegate = self
                ImageView[nekoNumber].addGestureRecognizer(panGesture)
                //Tap操作をImageViewに登録
                let tapGesture = UIPanGestureRecognizer(target: self, action: #selector(self.tapImage(sender:)))
                tapGesture.delegate = self
                ImageView[nekoNumber].addGestureRecognizer(tapGesture)
            }
            
        }
        //状態の確認
        print("imageX:\(imageX)")
        
        //=======================ランダム化============================================
        shuffleImageX = imageX.shuffled()
        print("shuffleImageX\(shuffleImageX)")
        for num in 0..<ImageView.count{
            ImageView[num].center.x = shuffleImageX[num]
            self.view.addSubview(ImageView[num])
        }
        
        
        
        
//        print("imageY:\(imageY)")
//        print("imageView.count:\(ImageView.count)")
        print("checkAndJudge:\(checkAndJudge)")
        
        
        
        let boxImage:UIImage = UIImage(named: "fish&nako can box_icon")!
        let boxImageView:UIImageView = UIImageView(image: boxImage)
        boxImageView.frame = CGRect(x: 840, y: 400, width: 110, height: 300)
        self.view.addSubview(boxImageView)
        
        var trueNumber = 0
        for check in checkCorrect!{
            if check == true{
                
                let fishAndNekoImageView:UIImageView = UIImageView(image: nekofish)
                fishAndNekoImageView.frame = CGRect(x: 861, y: 409 + 55 * trueNumber, width: 60, height: 60)
                self.view.addSubview(fishAndNekoImageView)
                trueNumber = trueNumber + 1
            }
        }
        
        
    }
    
    
    
    
    //音声ファイルを再生する時の関数
    func playSound(name: String) {
            guard let path = Bundle.main.path(forResource: name, ofType: "mp3") else {
                print("音源ファイルが見つかりません")
                return
            }
            
            do {
                // AVAudioPlayerのインスタンス化
                audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                
                // AVAudioPlayerのデリゲートをセット
                audioPlayer.delegate = self
                print("okok")
                // 音声の再生
                audioPlayer.play()
            } catch {
            }
    }
        
    
    
    
    
    
    
    func nextView(){
        //データベースに書き込み
        do{
            try realm.write {
                realm.add(entiredata)  // 作成した「realm」というインスタンスにrealmDataを書き込みます。
            }
        } catch {
            print("★☆注意データベース書き込みエラー☆★")
        }
        
        let storyboard: UIStoryboard = self.storyboard!
        let Voice = storyboard.instantiateViewController(withIdentifier: "Voice") as!VoiceViewController
        Voice.stageNumber = stageNumber!
        Voice.questionNumber = questionNumber!
        Voice.correctAnswer = correctAnswer!
        Voice.checkCorrect = checkCorrect!
        Voice.realm = realm
        Voice.id = id
        self.present(Voice, animated: true, completion: nil)
    }

    //=============================================================
    
    //PanとかTapとか関係======================================================
    //パンしている間ずっと繰り返し呼ばれる関数．
    @objc func panMove(sender: UIPanGestureRecognizer) {
        
        
        //sender.view = 選択したUIImageViewのこと
        
        //移動した位置の座標を取得
        let point: CGPoint = sender.translation(in: self.view)
        //移動した座標のx,y座標を中心にする
        let movePoint = CGPoint(x: (sender.view?.center.x)! + point.x, y: (sender.view?.center.y)! + point.y)
        sender.view?.center = movePoint
        //移動距離の初期化
        sender.setTranslation(CGPoint.zero, in: self.view)
        
        //パンが終わった時の処理はここ
        if sender.state == UIGestureRecognizer.State.ended {
            print("移動終了")
            if (sender.view?.center.y)! < 400{
                var dMin:Double = -1
                var willMovePoint = CGPoint()
                var blankTag = -1
                //猫と空欄が一番近いところを調べる
                for blank in blankView.subviews {
                    let distance_x = Double(blank.center.x - (sender.view?.center.x)!)
                    let distance_y = Double(blank.center.y - (sender.view?.center.y)!)
                    let distance:Double = sqrt( distance_x*distance_x + distance_y*distance_y )
                    let distance_tag = blank.tag
                    if dMin == -1{
                        dMin = distance
                        willMovePoint = blank.center
                        blankTag = distance_tag
                    }
                    
                    if distance < dMin {
                        dMin = distance
                        willMovePoint = blank.center
                        blankTag = blank.tag
                    }
                }
                willMovePoint.x = willMovePoint.x - 10
                willMovePoint.y = willMovePoint.y - 10
                sender.view?.center = willMovePoint
                
//                imageX[(sender.view?.tag)!] = willMovePoint.x
//                imageY[(sender.view?.tag)!] = willMovePoint.y
                
                print("猫の画像の\((sender.view?.tag)!)番目を空白の\(blankTag)番目に入れました")
                
                if (sender.view?.tag)! == blankTag {
                    checkAndJudge[(sender.view?.tag)!][0] = 1
                    checkAndJudge[(sender.view?.tag)!][1] = checkAndJudge[(sender.view?.tag)!][1] + 1
                }else{
                    
                    
                    
                    //もし間違いだったら1秒後にもとにもどす.
                    if checkAndJudge[(sender.view?.tag)!][0] == 0{
                        //猫の画像を入れ替えたり、猫を泣かせたりする。
                        print("間違いだよ")
                        ImageView[sender.view!.tag].image = UIImage(named: "komari_cat")
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
                            self.playSound(name: "sound_cat/komari cat_audio")
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                //非同期処理でやります。3秒後にやりたい処理をここへ。
                                //同期処理でやりたいならそれ用で。
                                print("1秒後")
                                sender.view?.center = CGPoint(x: self.shuffleImageX[sender.view!.tag], y:self.imageY[sender.view!.tag] )
//                                self.ImageView[sender.view!.tag].image = UIImage(named: "Stage \(self.stageNumber!)_Q\(self.questionNumber!)/cat_S\(self.stageNumber!)_Q\(self.questionNumber!)_\(sender.view!.tag + 1).png")!
                                self.ImageView[sender.view!.tag].image = UIImage(named: "Neko_Chunk_Cats _NEW/Stage \(self.stageNumber!)_Q\(self.questionNumber!)_\(sender.view!.tag + 1).png")!
                            }
            
                           
                        }
                    }
                    
                    
                    
                    
                    
                    
                    
                    checkAndJudge[(sender.view?.tag)!][0] = 0
                    checkAndJudge[(sender.view?.tag)!][1] = checkAndJudge[(sender.view?.tag)!][1] + 1
                    

                }
                correctjudge()
                
                
            }else{
                if checkAndJudge[(sender.view?.tag)!][0] == 1{
                    checkAndJudge[(sender.view?.tag)!][0] = 0
                }
                
            }
            
            print("checkAndJudge:\(checkAndJudge)")
        
            
        }
    }
    

    
    
    func correctjudge(){
        for item in checkAndJudge {
            if item[0] != 1 {
                print("まだまだ")
                return
            }
        }
        print("正解")
        seikai()
    }
    func seikai() {
        correctAnswer = correctAnswer! + 1
        entiredata.time = "\(TimeManager.questionseconds)"
        entiredata.judge = "◯"
        entiredata.catinfo = entiredata.setCatInfo(catinfo: checkAndJudge)
        checkCorrect?[questionNumber! - 1] = true
        nextView()
    }
    
    func huseikai() {
        entiredata.time = "\(TimeManager.questionseconds)"
        entiredata.judge = "☓"
        entiredata.catinfo = entiredata.setCatInfo(catinfo: checkAndJudge)
        nextView()
    }
    
    
    
    
    //画像をタップするごとに呼ばれる関数
    @objc func tapImage(sender: UIPanGestureRecognizer) {
        //選択した画像を前に表示(なんかいまいち動作がうまくいかない．動かさないと前に来ない)
        self.view.bringSubviewToFront(sender.view!)
    }
    //==========================================================================================
    
    
    
    //UIGestureRecognizerによるDelegate関数======================================================
    //PanやTapの競合をなくすためにつけとくおまじない
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    //複数のジェスチャーを同時に使うことを許可する
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
    
    //=============================================================
    
    
    

    //TimeManagerDelegate============================
    //問題の時間関係
    func timeSection(seconds: Int) {
        
        if seconds == 30{
            Nekocan1.removeFromSuperview()
            entiredata.judge = "☓"
            entiredata.time = "30"
            entiredata.catinfo = entiredata.setCatInfo(catinfo: checkAndJudge)
            
            nextView()
        }else{
            var minusalpha = CGFloat(seconds) / 30
            Nekocan1.alpha = CGFloat(1 - minusalpha)
        }
    }
    //===============================================
    
    //アプリが閉じようとしている時の処理======================================================
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        // timerが起動中なら一旦破棄する
        if TimeManager.questiontimer != nil{
            TimeManager.questiontimer.invalidate()
        }
        if TimeManager.logtimer != nil{
            TimeManager.questiontimer.invalidate()
        }
    }
    //==========================================================================================
    
}
