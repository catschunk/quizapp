//
//  ResultsViewController.swift
//  catStage
//
//  Created by はすゆういち on 2019/05/05.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit
import AVFoundation

class ResultsViewController: UIViewController,AVAudioPlayerDelegate {
    //音声再生用
    var audioPlayer: AVAudioPlayer!
    
    
    var id:String?
    var stageNumber:Int?
    var questionNumber:Int?
    var correctAnswer:Int?
    var checkCorrect:[Bool]?
//    var TimeManager:TimerClass!
    
    var nekofish:UIImage!
    
    let stageOneAnswer = [
        "Lake District is a very beautiful place.",
        "I want to play the drums like you.",
        "I have nothing special to do today.",
        "I did my math homework while I was listening to music.",
        "I couldn't find the CD I was looking for."
    ]
    
    let stageTwoAnswer = [
        "Ichiro is a very famous baseball player.",
        "I want to draw pictures like you.",
        "I have nothing important to tell you.",
        "I listened to music while I was taking a bath.",
        "I couldn't find the book I was looking for."
    ]

    
    
    @IBAction func nextPage(_ sender: Any) {
        nextView()
    }
    @IBOutlet weak var stageName: UIImageView!
    
    @objc func speakEvent(_ sender: UIButton) {
        print(sender.tag)
        print("Stage\(stageNumber!)_audio_new/Stage\(stageNumber!)_Q\(sender.tag + 1)")
        playSound(name: "Stage\(stageNumber!)_audio_new/Stage\(stageNumber!)_Q\(sender.tag + 1)")
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("checkCorrect:\(checkCorrect!)")
        setImage()

        
    }
    func nextView(){
        let storyboard: UIStoryboard = self.storyboard!
//        if correctAnswer! == 5 {
            let Congratulations = storyboard.instantiateViewController(withIdentifier: "Congratulations") as!CongratulationsViewController
            Congratulations.id = id!
            Congratulations.stageNumber = stageNumber!
            Congratulations.questionNumber = questionNumber!
            Congratulations.correctAnswer = correctAnswer!
            self.present(Congratulations, animated: true, completion: nil)
            
//        }else{
//            let Stage = storyboard.instantiateViewController(withIdentifier: "Stage") as!StageViewController
//            self.present(Stage, animated: true, completion: nil)
//        }
    }
    func setImage(){
        stageName.image = UIImage(named: "Stage \(stageNumber!)_icon_small")
        var labelView:[UIImageView] = []
        var EnglishText:[UILabel] = []
        var speaker:[UIButton] = []
        
        
        
        
        
        for N in 0...4{
            //英文の枠を設置
            labelView.append(UIImageView(image: UIImage(named: "Results page_sentence box")))
            labelView[N].frame = CGRect(x: 120, y: 233 + 57 * N, width: 750, height: 55)
            self.view.addSubview(labelView[N])
            EnglishText.append(UILabel())
            //英文を設置
            EnglishText[N].font = UIFont.boldSystemFont(ofSize: 23)
            if stageNumber == 1 {
                EnglishText[N].text = stageOneAnswer[N]
            }else{
                EnglishText[N].text = stageTwoAnswer[N]
            }
            EnglishText[N].frame = CGRect(x: 180, y: 247 + 57 * N, width: 0, height: 52)
            EnglishText[N].sizeToFit()
            self.view.addSubview(EnglishText[N])
            //speaker
            speaker.append(UIButton())
            speaker[N].setImage(UIImage(named: "speaker")!, for: .normal)
            speaker[N].frame = CGRect(x: 820, y: 240 + 57 * N, width: 40, height: 45)
            speaker[N].addTarget(self, action:#selector(speakEvent(_:)), for: UIControl.Event.touchUpInside)
            speaker[N].tag = N
            self.view.addSubview(speaker[N])
            
            
            
        }
        
        
        var trueNumber = 0
        for check in checkCorrect!{
            if check == true{
                if stageNumber! == 1 {
                    nekofish = UIImage(named: "fish.png")
                }else{
                    nekofish = UIImage(named: "neko can.png")
                }
                
                let fishAndNekoImageView:UIImageView = UIImageView(image: nekofish)
                fishAndNekoImageView.frame = CGRect(x: 103, y: 228 + 57 * trueNumber, width: 60, height: 60)
                self.view.addSubview(fishAndNekoImageView)
            }
            trueNumber = trueNumber + 1
        }
        
        
        
        
        
        
    }
    
    //音声ファイルを再生する時の関数
    func playSound(name: String) {
        guard let path = Bundle.main.path(forResource: name, ofType: "mp3") else {
            print("音源ファイルが見つかりません")
            return
        }
        
        do {
            // AVAudioPlayerのインスタンス化
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            
            // AVAudioPlayerのデリゲートをセット
            audioPlayer.delegate = self
            print("okok")
            // 音声の再生
            audioPlayer.play()
        } catch {
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



