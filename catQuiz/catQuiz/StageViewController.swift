//
//  StageViewController.swift
//  catQuiz
//
//  Created by はすゆういち on 2019/05/01.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit

class StageViewController: UIViewController {
    //ログ用のデータ
    var entiredata = entireData()
    var id:String?
    var stageNumber = 0

    @IBAction func Stage1Button(_ sender: Any) {
        stageNumber = 1
        
    }
    @IBAction func Stage2Button(_ sender: Any) {
        stageNumber = 2
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        


        // Do any additional setup after loading the view.
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let DescriptionView = segue.destination as! DescriptionViewController
        DescriptionView.stageNumber = stageNumber
        DescriptionView.entiredata = entiredata
        DescriptionView.id = id
    }

}
