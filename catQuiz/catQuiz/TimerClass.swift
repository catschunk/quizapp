//
//  TimerClass.swift
//  catQuiz
//
//  Created by 保浦良太 on 2019/03/25.
//  Copyright © 2019年 lab. All rights reserved.
//

import UIKit


class TimerClass: NSObject {
    //timer:タイマークラス  startTime:初めた時刻　　seconds:かかった時間
    var logtimer: Timer!
    var questiontimer: Timer!
    
    var startTime: String = ""
    var questionseconds: Int = 0
    var timedelegate:TimeManageDelegate!
    
    //ステージで30秒図る時の用の時間========================
    func startQuestionTimer() {
        
        if questiontimer != nil{
            // timerが起動中なら一旦破棄する
            questiontimer.invalidate()
        }
        //timerの設定
        //timeInterval:間隔，selector:間隔ごとに呼び出されるメソッド
        questiontimer = Timer.scheduledTimer(
            timeInterval: 1,
            target: self,
            selector: #selector(self.timerCounter),
            userInfo: nil,
            repeats: true)
    }
    
    //selector
    @objc func timerCounter() {
        questionseconds = questionseconds + 1
        print(questionseconds)
        timedelegate.timeSection(seconds: questionseconds)
    }
    //=================================================
    
    func nowtimeGet() -> String {
        //時刻のフォーマット作成 例)"2019/03/21"
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .short
        formatter.locale = Locale(identifier: "ja_JP")
        let now = Date()
        startTime = formatter.string(from: now)
        
        return startTime
    }
    
}

