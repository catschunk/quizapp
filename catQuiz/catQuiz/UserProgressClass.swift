//
//  LogModel.swift
//  catQuiz
//
//  Created by 保浦良太 on 2019/03/27.
//  Copyright © 2019年 lab. All rights reserved.
//

import Foundation
import RealmSwift

class progressData: Object{
    @objc dynamic var stage: String = "" //ステージ
    @objc dynamic var question: String = "" //問題
    @objc dynamic var judge: String = ""
    @objc dynamic var seconds: String = "" //かかった時間
    @objc dynamic var trialsNumber: Int = 1
}

class entireData:Object{
    @objc dynamic var startTime: String = ""  //開始時刻
    @objc dynamic var time:String = ""
    @objc dynamic var id: String = ""  //id
    @objc dynamic var stage: String = "" //ステージ
    @objc dynamic var question: String = "" //問題
    @objc dynamic var judge: String = ""  //最終的な正答
    @objc dynamic var catinfo:String = ""
    
    
    func setCatInfo(catinfo:[[Int]]) ->String {
        var info = ""
        var str = ""
        var i = 1
        for cat in catinfo{
            //最後だけ末尾変える
            if catinfo.count == i {
                str = "猫\(i):\(cat[1])回"
            }else{
                str = "猫\(i):\(cat[1])回，"
            }
            info = info + str
            i = i + 1
        }
        return info
        
    }
    
}
