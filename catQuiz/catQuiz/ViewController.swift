//
//  ViewController.swift
//  catQuiz
//
//  Created by はすゆういち on 2019/03/09.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI

class ViewController: UIViewController ,MFMailComposeViewControllerDelegate{
    var realm = try! Realm()
    
    //スタートボタンの画面遷移
    @IBAction func startButton(_ sender: Any) {
        let storyboard: UIStoryboard = self.storyboard!
        let nextView = storyboard.instantiateViewController(withIdentifier: "IDCheck") //次のViewをStoryBoardIDで設定
        self.present(nextView, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("RealmURL = \(realm.configuration.fileURL)")
    }
    
    //ログ送信ボタン
    
    @IBAction func SendLogBtnTapped(_ sender: Any) {
        //メールを送信できるかチェック
        if MFMailComposeViewController.canSendMail()==false {
            print("Email Send Failed")
            return
        }
        sendMailWithCSV(subject: "ログ", message: "このまま送信してください。")
    }
    
    func sendMailWithCSV(subject: String, message: String) {
        let mailViewController = MFMailComposeViewController()
        mailViewController.mailComposeDelegate = self
//        var toRecipients = ["drgunz112@gmail.com"]
        var toRecipients = ["hassu1230.snoopy@gmail.com"]
        mailViewController.setSubject(subject)
        mailViewController.setToRecipients(toRecipients)
        mailViewController.setMessageBody(message, isHTML: false)
        do {
            //realmファイルの送信
            //TODO: mimeTypeの設定 "text/csv" ? (バイナリフィルの指定がわからない...)
            let data = try Data(contentsOf: realm.configuration.fileURL!)
            mailViewController.addAttachmentData(data, mimeType: "realm/binary", fileName: "default.realm")
        } catch {
            print("error ...")
        }
        self.present(mailViewController,animated: true)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result {
        case .sent:
            break
        default:
            break
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    
   

}

