//
//  VoiceViewController.swift
//  catQuiz
//
//  Created by はすゆういち on 2019/05/04.
//  Copyright © 2019 lab. All rights reserved.
//

import UIKit
import RealmSwift
import AVFoundation

class VoiceViewController: UIViewController, AVAudioPlayerDelegate{

    //音声再生用
    var audioPlayer: AVAudioPlayer!
    
    var id:String?
    var stageNumber:Int?
    var questionNumber:Int?
    var correctAnswer:Int?
    var checkCorrect:[Bool]?
//    var TimeManager:TimerClass!
    var realm:Realm!
    @IBOutlet weak var EnglishText: UILabel!
    @IBOutlet weak var JapanText: UILabel!
    
    var nekofish:UIImage!
    
    let JapaneseSentence1 = [
        "湖水地方はとても美しい場所です。",
        "私はあなたのようにドラムを演奏したいです。",
        "私は今日，特にすることがありません。",
        "私は音楽を聴きながら数学の宿題をやりました。",
        "私は探していたCDを見つけることができませんでした。"]
    
    let EnglishSentence1 = [
        "Q1: Lake District is a very beautiful place.",
        "Q2: I want to play the drums like you.",
        "Q3: I have nothing special to do today.",
        "Q4: I did my math homework while I was listening to music.",
        "Q5: I couldn't find the CD I was looking for."
    ]
    
    let JapaneseSentence2 = [
        "イチローはとても有名な野球選手です。",
        "私はあなたのように上手に絵を描きたいです。",
        "あなたに伝えるべき大事なことはありません。",
        "私は風呂に入りながら音楽を聴きました。",
        "私は探していた本を見つけることができませんでした。"
    ]
    
    let EnglishSentence2 = [
        "Q1: Ichiro is a very famouse baseball player.",
        "Q2: I want to draw pictures like you.",
        "Q3: I have nothing important to tell you.",
        "Q4: I listened to music while I was taking a bath.",
        "Q5: I couldn't find the book I was looking for."
    ]
    
    @IBAction func Voice(_ sender: Any) {
        print("音声ボタンが押されました。")
        playSound(name: "Stage\(stageNumber!)_audio_new/Stage\(stageNumber!)_Q\(questionNumber!)")
    }
    
    @IBAction func nextPage(_ sender: Any) {
        nextView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setImage()
    }
    func setImage(){
//        var Jsentence:[String] = []
//        var Esentence:[String] = []
//
        //コード短めにするために切り替える
        switch stageNumber {
        case 1:
            JapanText.text = JapaneseSentence1[questionNumber! - 1]
            EnglishText.text = EnglishSentence1[questionNumber! - 1]
            nekofish = UIImage(named: "fish.png")
//            Jsentence = JapaneseSentence1
//            Esentence = EnglishSentence1
        case 2:
            JapanText.text = JapaneseSentence2[questionNumber! - 1]
            EnglishText.text = EnglishSentence2[questionNumber! - 1]
            nekofish = UIImage(named: "neko can.png")
//            Jsentence = JapaneseSentence2
//            Esentence = EnglishSentence2
        default: break
        }
        
        
        let boxImage:UIImage = UIImage(named: "fish&nako can box_icon")!
        let boxImageView:UIImageView = UIImageView(image: boxImage)
        boxImageView.frame = CGRect(x: 840, y: 400, width: 110, height: 300)
        self.view.addSubview(boxImageView)
        
        var trueNumber = 0
        for check in checkCorrect!{
            if check == true{
                let fishAndNekoImageView:UIImageView = UIImageView(image: nekofish)
                fishAndNekoImageView.frame = CGRect(x: 861, y: 409 + 55 * trueNumber, width: 60, height: 60)
                self.view.addSubview(fishAndNekoImageView)
                trueNumber = trueNumber + 1
            }
        }
        
    }
    
    //音声ファイルを再生する時の関数
    func playSound(name: String) {
        guard let path = Bundle.main.path(forResource: name, ofType: "mp3") else {
            print("音源ファイルが見つかりません")
            return
        }
        
        do {
            // AVAudioPlayerのインスタンス化
            audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
            
            // AVAudioPlayerのデリゲートをセット
            audioPlayer.delegate = self
            print("okok")
            // 音声の再生
            audioPlayer.play()
        } catch {
        }
    }
    
    

    func nextView(){
        let storyboard: UIStoryboard = self.storyboard!
        if questionNumber! == 5 {
            let Results = storyboard.instantiateViewController(withIdentifier: "Results") as!ResultsViewController
            Results.stageNumber = stageNumber!
            Results.questionNumber = questionNumber!
            Results.correctAnswer = correctAnswer!
            Results.checkCorrect = checkCorrect!
            Results.id = id
            self.present(Results, animated: true, completion: nil)
            
        }else{
            let Quiz = storyboard.instantiateViewController(withIdentifier: "Quiz") as!QuizViewController
            Quiz.stageNumber = stageNumber!
            Quiz.questionNumber = questionNumber!
            Quiz.correctAnswer = correctAnswer!
            Quiz.checkCorrect = checkCorrect!
            //時間関係
//            Quiz.TimeManager = TimeManager
            Quiz.firstFlag = false
            Quiz.realm = realm
            Quiz.id = id
            self.present(Quiz, animated: true, completion: nil)
        }
    }
}
